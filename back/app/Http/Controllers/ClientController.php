<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

use App\Http\Requests\CreateClientRequest;

class ClientController extends Controller
{

    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return response()->json([
            'message' => 'List all Clients ',
            'client' => $clients,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateClientRequest $request)
    {
        $client = Client::create($request->all());
        return response()->json([
            'message' => 'Client successfully registered',
            'client' => $client,
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function kpideclientes()
    {
        $value = Client::all();
        $avg = $value->avg('age');
        return response()->json([
            'message' => 'Client average age',
            'client' => $avg,
        ], 201);
    }

}
