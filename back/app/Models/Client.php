<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use \Carbon\Carbon;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'last_name',
        'birthday',
    ];
    
    protected $appends =[
        'age'
    ];

    public function getAgeAttribute()
    {
        return Carbon::parse($this->birthday)->age;
    }

    public function setBirthdayAttribute($value) {
        $this->attributes['birthday'] = 
            Carbon::parse($value)->format('Y-m-d H:i:s') ;
    }

}
