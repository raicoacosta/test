<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/hola', function() {
//             return response()->json([
//             'message' => 'User successfully registered',
//         ], 201);
 
// });

Route::group([
    'middleware' => 'api',

], function ($router) {
    Route::group([
        'prefix' => 'auth'
    
    ], function ($router) {
        Route::post('/login', [AuthController::class, 'login']);
        Route::post('/register', [AuthController::class, 'register']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/refresh', [AuthController::class, 'refresh']);
    });

    Route::group([
        'prefix' => 'client'
    ], function ($router) {
        Route::get('/listclientes', [ClientController::class, 'index']);
        Route::post('/creacliente', [ClientController::class, 'store']);
        Route::get('/kpideclientes', [ClientController::class, 'kpideclientes']);
    });
});